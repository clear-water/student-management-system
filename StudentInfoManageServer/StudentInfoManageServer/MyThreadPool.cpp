#include "MyThreadPool.h"



CMyThreadPool::CMyThreadPool()
{
}


CMyThreadPool::~CMyThreadPool()
{
}

//创建等待线程
void CMyThreadPool::CreateThreadPool()
{
    SYSTEM_INFO SysInfo;
    //获取cpu核心数
    ::GetSystemInfo(&SysInfo);

    //创建对应的信号量
    m_Semaphore.Cretae(0, SysInfo.dwNumberOfProcessors);

    //创建对应的线程
    for (int i = 0; i < SysInfo.dwNumberOfProcessors; i++)
    {
        HANDLE hThread = CreateThread(NULL, 0, WorkThread, this, 0, NULL);
        CloseHandle(hThread);
    }
}

//向任务队列添加任务
void CMyThreadPool::InsertTask(CMyTask *pTask)
{
    //添加到任务队列
    m_CS.Enter();
    m_QueueTask.push(pTask);
    m_CS.Leave();

    //释放一个信号
    m_Semaphore.Release(1);
}

//工作线程
DWORD WINAPI CMyThreadPool::WorkThread(LPVOID lpParameter)
{
    CMyThreadPool *pThis = (CMyThreadPool *)lpParameter;

    while (true)
    {
        //等待任务来临
        pThis->m_Semaphore.WiteForSemaphore();

        pThis->m_CS.Enter();
        //从任务队列取出任务
        CMyTask *pTask = pThis->m_QueueTask.front();
        //弹出任务队列中的任务
        pThis->m_QueueTask.pop();
        pThis->m_CS.Leave();

        //执行任务
        pTask->Execute();
    }
}
